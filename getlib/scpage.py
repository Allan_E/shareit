def load(self, headerTemplate, footerTemplate, subcatTemplate, db, sid):
    uname = self.get_cookie('username', '')
    storedRemkey = self.get_cookie('remkey', '')
    dbRemkey = ""

    for i in db.query("SELECT remkey FROM users WHERE username=%s LIMIT 1", uname):
        dbRemkey = i.remkey

    if uname == '' or storedRemkey != dbRemkey:
        self.write(headerTemplate.generate(username="", db=db))
    else:
        self.write(headerTemplate.generate(username=uname, db=db))

    self.write(subcatTemplate.generate(db=db, sname=sid))
    self.finish(footerTemplate.generate())


def create(self, headerTemplate, footerTemplate, createSubcatTemplate, db):
    uname = self.get_cookie('username', '')
    storedRemkey = self.get_cookie('remkey', '')
    dbRemkey = ""
    err = self.get_argument('err', '')
    for i in db.query("SELECT remkey FROM users WHERE username=%s LIMIT 1", uname):
        dbRemkey = i.remkey

    if uname == '' or storedRemkey != dbRemkey:
        self.write(headerTemplate.generate(username="", db=db))
        self.write(createSubcatTemplate.generate(username="", err=err))
    else:
        self.write(headerTemplate.generate(username=uname, db=db))
        self.write(createSubcatTemplate.generate(username=uname, err=err))

    self.finish(footerTemplate.generate())