def index(self, headerTemplate, footerTemplate, contentTemplate, db):
        uname = self.get_cookie('username', '')
        storedRemkey = self.get_cookie('remkey', '')
        dbRemkey = ""

        for i in db.query("SELECT remkey FROM users WHERE username=%s LIMIT 1", uname):
            dbRemkey = i.remkey

        if uname == '':
            self.write(headerTemplate.generate(username="", db=db))
            self.write(contentTemplate.generate(db=db, username=""))
        elif storedRemkey != dbRemkey:
            self.write(headerTemplate.generate(username="", db=db))
            self.write(contentTemplate.generate(db=db, username=""))
        else:
            self.write(headerTemplate.generate(username=uname, db=db))
            self.write(contentTemplate.generate(db=db, username=uname))
        self.finish(footerTemplate.generate())


def submitpage(self, headerTemplate, footerTemplate, submitTemplate, db):
        uname = self.get_cookie('username', '')
        storedRemkey = self.get_cookie('remkey', '')
        dbRemkey = ""

        for i in db.query("SELECT remkey FROM users WHERE username=%s LIMIT 1", uname):
            dbRemkey = i.remkey

        if uname == '' or storedRemkey != dbRemkey:
            self.write(headerTemplate.generate(username="", db=db))
            self.write(submitTemplate.generate(db=db, username=""))
        else:
            self.write(headerTemplate.generate(username=uname, db=db))
            self.write(submitTemplate.generate(db=db, username=uname))
        self.finish(footerTemplate.generate())