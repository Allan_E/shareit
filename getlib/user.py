def login(self, headerTemplate, footerTemplate, loginTemplate, db):
        err = self.get_argument('err', '')

        if self.get_cookie('username'):
            self.redirect("/")
        else:
            self.write(headerTemplate.generate(username="", db=db))
            self.write(loginTemplate.generate(err=err))
            self.finish(footerTemplate.generate())


def profile(self, headerTemplate, footerTemplate, profileTemplate, db, username):
        uname = self.get_cookie('username', '')

        cmtCnt = db.execute_rowcount("SELECT * FROM comments WHERE author=%s", username)

        storedRemkey = self.get_cookie('remkey', '')
        dbRemkey = ""

        for i in db.query("SELECT remkey FROM users WHERE username=%s LIMIT 1", uname):
            dbRemkey = i.remkey

        if uname == '' or storedRemkey != dbRemkey:
            self.write(headerTemplate.generate(username="", db=db))
        else:
            self.write(headerTemplate.generate(username=uname, db=db))
        self.write(profileTemplate.generate(db=db, username=username, uname=uname, cmtCnt=cmtCnt))
        self.finish(footerTemplate.generate())

def logout(self):
        self.clear_cookie("username")
        self.clear_cookie("remkey")
        self.redirect("/")