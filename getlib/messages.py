def draw(self, headerTemplate, footerTemplate, messagesTemplate, db):
    uname = self.get_cookie('username', '')
    storedremkey = self.get_cookie('remkey', '')
    dbremkey = ""

    for i in db.query("SELECT remkey FROM users WHERE username = %s", uname):
        dbremkey = i.remkey

    if storedremkey != dbremkey:
        self.write(headerTemplate.generate(username="", db=db))
        self.write(messagesTemplate.generate(username="", db=db))
    else:
        self.write(headerTemplate.generate(username=uname, db=db))
        self.write(messagesTemplate.generate(username=uname, db=db))
    self.finish(footerTemplate.generate())