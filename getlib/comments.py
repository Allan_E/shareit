def draw(self, headerTemplate, footerTemplate, commentTemplate, db, pid):
        error = self.get_argument('err', '')
        if error:
            err = 1
        else:
            err = 0

        if pid:
            pid = int(pid)
        else:
            pid = 0

        cmtCnt = db.execute_rowcount("SELECT * FROM comments WHERE pid=%s AND deleted=0", pid)

        if cmtCnt > 1:
            commentText = "Comments"
        elif cmtCnt == 0:
            commentText = "Comments"
        else:
            commentText = "Comment"

        uname = self.get_cookie('username', '')
        storedRemkey = self.get_cookie('remkey', '')
        dbRemkey = ""

        for i in db.query("SELECT remkey FROM users WHERE username=%s LIMIT 1", uname):
            dbRemkey = i.remkey

        if uname == '' or storedRemkey != dbRemkey:
            self.write(headerTemplate.generate(username="", db=db))
            self.write(commentTemplate.generate(db=db, pid=pid, err=err, cmtCnt=cmtCnt, commentText=commentText, username=""))
        else:
            self.write(headerTemplate.generate(username=uname, db=db))
            self.write(commentTemplate.generate(db=db, pid=pid, err=err, cmtCnt=cmtCnt, commentText=commentText, username=uname))
        self.finish(footerTemplate.generate())