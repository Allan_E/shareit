function openComments() {
	$('#cmtForm').css('display', 'block');
	$('#OCFbtn').css('display', 'none');
}
function closeComments() {
	$('#cmtForm').css('display', 'none');
	$('#OCFbtn').css('display', 'block');
}
function upvote(pid){
	$.ajax({
  		type: "POST",
  		url: "/upvote",
  		data: { pid: pid }
	}).done(function( msg ) {
    	$('.vc-' + pid).html(msg);
      $('.uv-' + pid).attr("disabled", "true");
  	});
}
function downvote(pid){
	$.ajax({
  		type: "POST",
  		url: "/downvote",
  		data: { pid: pid }
	}).done(function( msg ) {
    	$('.vc-' + pid).html(msg);
      $('.dv-' + pid).attr("disabled", "true");
  	});
}
function openReply(parent){
    $('.reply-' + parent).css('display', 'block');
    $('.reply-button-' + parent).css('display', 'none');
}
function closeReply(parent){
    $('.reply-' + parent).css('display', 'none');
    $('.reply-button-' + parent).css('display', 'block');
}