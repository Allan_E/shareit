import os
import tornado.template
import tornado.httpserver
import tornado.ioloop
import tornado.web
import torndb
from confmodel import hdr, portconfig
from config import *
from admin import admin
from postlib import subcat, delete, login, signup, submit, vote, msg
from getlib import draw, comments, user, scpage, messages

"""Determine whether we want to run a production or development build"""


def listen():
    if state == "development":
        http_server.listen(devPort)
    elif state == "production":
        http_server.listen(productionPort)
    else:
        port = "Error, please set whether this is a production or development instance."
        return port


"""Define static file paths"""
style_path = os.path.join(os.getcwd(), 'css')
img_path = os.path.join(os.getcwd(), 'images')
js_path = os.path.join(os.getcwd(), 'js')
fonts_path = os.path.join(os.getcwd(), 'fonts')
favicon_path = os.getcwd()
 
"""Load the templates as raw text"""
headerRaw = open("templates/header.html").read()
footerRaw = open("templates/footer.html").read()
contentRaw = open("templates/content.html").read()
submitRaw = open("templates/submit.html").read()
commentRaw = open("templates/comment.html").read()
loginRaw = open("templates/login.html").read()
profileRaw = open("templates/profile.html").read()
subcatRaw = open("templates/subcat.html").read()
createSubcatRaw = open("templates/createsubcat.html").read()
messagesRaw = open("templates/messages.html").read()

"""Get and compile the templates"""
headerTemplate = tornado.template.Template(headerRaw)
footerTemplate = tornado.template.Template(footerRaw)
contentTemplate = tornado.template.Template(contentRaw)
submitTemplate = tornado.template.Template(submitRaw)
commentTemplate = tornado.template.Template(commentRaw)
loginTemplate = tornado.template.Template(loginRaw)
profileTemplate = tornado.template.Template(profileRaw)
subcatTemplate = tornado.template.Template(subcatRaw)
createSubcatTemplate = tornado.template.Template(createSubcatRaw)
messagesTemplate = tornado.template.Template(messagesRaw)

"""Connect to the database"""
db = torndb.Connection(db_host, db_db, db_user, db_password)

"""Index page handler"""


class IndexHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        hdr.set_header(self)

    def get(self):
        draw.index(self, headerTemplate, footerTemplate, contentTemplate, db)

"""Submit page handler"""


class SubmitLinkHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        hdr.set_header(self)

    def get(self):
        draw.submitpage(self, headerTemplate, footerTemplate, submitTemplate, db)

"""Add link to database handler"""


class PostLink(tornado.web.RequestHandler):
    def set_default_headers(self):
        hdr.set_header(self)

    def post(self):
        submit.link(self, db)

"""Verify and delete post"""


class DeletePostHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        hdr.set_header(self)

    def get(self):
        delete.post(self, db)


"""Display the comments page"""


class CommentHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        hdr.set_header(self)

    def get(self, pid):
        comments.draw(self, headerTemplate, footerTemplate, commentTemplate, db, pid)

"""Add a comment to the database"""


class SubmitCommentHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        hdr.set_header(self)

    def post(self):
        submit.comment(self, db)

class DeleteCommentHandler(tornado.web.RequestHandler):
    def get(self):
        delete.comment(self, db)

"""Present the login/signup page"""


class LoginPageHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        hdr.set_header(self)

    def get(self):
        user.login(self, headerTemplate, footerTemplate, loginTemplate, db)

"""Signup handler. Gets username/password, hashes password and adds user to the database"""


class SignupHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        hdr.set_header(self)

    def post(self):
        signup.signup(self, db)

"""Logs user in"""


class LoginHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        hdr.set_header(self)

    def post(self):
        login.login(self, db)

"""Logout the user"""


class LogoutHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        hdr.set_header(self)

    def get(self):
        user.logout(self)

"""Draws the users profile page"""


class ProfileHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        hdr.set_header(self)

    def get(self, username):
        user.profile(self, headerTemplate, footerTemplate, profileTemplate, db, username)

""" Upvote and downvote handlers """


class UpVoteHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        hdr.set_header(self)

    def post(self):
        vote.up(self, db)


class DownVoteHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        hdr.set_header(self)

    def post(self):
        vote.down(self, db)

"""Subcat stuff"""
"""Create a new subcat"""


class NewSubcat(tornado.web.RequestHandler):
    def set_default_headers(self):
        hdr.set_header(self)

    def post(self):
        subcat.create(self, db)

"""Render subcat name"""


class SubcatPageHandler(tornado.web.RequestHandler):
    def get(self, sid):
        scpage.load(self, headerTemplate, footerTemplate, subcatTemplate, db, sid)

"""Draw the subcat create page"""


class SubcatCreatePage(tornado.web.RequestHandler):
    def get(self):
        scpage.create(self, headerTemplate, footerTemplate, createSubcatTemplate, db)

"""Admin stuff"""

"""Draw the admin page"""


class AdminPage(tornado.web.RequestHandler):
    def set_default_headers(self):
        hdr.set_header(self)

    def get(self, action):
        admin.loadPage(self, headerTemplate, footerTemplate, action, db)


class AddAdminHandler(tornado.web.RequestHandler):
    def post(self):
        tobeadded = self.get_argument('name', '')
        username = self.get_cookie('username', '')
        storedremkey = self.get_cookie('remkey', '')
        dbremkey = ""

        for i in db.query("SELECT remkey FROM users WHERE username=%s", username):
            dbremkey = i.remkey

        if storedremkey != dbremkey:
            self.redirect("/admin/main?err=2")
        elif tobeadded == "":
            self.redirect("/admin/main?err=2")
        else:
            db.execute("UPDATE users SET admin='1' WHERE username=%s", tobeadded)
            self.redirect("/admin/main")


class DefaultAdminHandler(tornado.web.RequestHandler):
    def get(self):
        self.redirect("/admin/main")

"""Message related stuff"""

class MessagePage(tornado.web.RequestHandler):
    def get(self):
        messages.draw(self, headerTemplate, footerTemplate, messagesTemplate, db)


class SendMessage(tornado.web.RequestHandler):
    def post(self):
        msg.send(self, db)

"""Error handlers"""
#TODO: Add error handlers

handlers = [
    #main page handler
    (r"/", IndexHandler),
    #submit link page and code handlers / delete post handlers
    (r"/submit", SubmitLinkHandler),
    (r"/submitlink", PostLink),
    (r"/delete", DeletePostHandler),
    #comment link page and code handlers
    (r"/comments/(?P<pid>[^\/]+)?", CommentHandler),
    (r"/submitcomment", SubmitCommentHandler),
    (r"/deletecomment", DeleteCommentHandler),
    #user system handlers
    (r"/login", LoginPageHandler),
    (r"/signup", SignupHandler),
    (r"/lg", LoginHandler),
    (r"/logout", LogoutHandler),
    (r"/user/(?P<username>[^\/]+)?", ProfileHandler),
    #vote handlers
    (r"/upvote", UpVoteHandler),
    (r"/downvote", DownVoteHandler),
    #subcat handlers
    (r"/createsubcat", NewSubcat),
    (r"/newcat", SubcatCreatePage),
    (r"/s/(?P<sid>[^\/]+)?", SubcatPageHandler),
    #admin handlers
    (r"/admin", DefaultAdminHandler),
    (r"/admin/add", AddAdminHandler),
    (r"/admin/(?P<action>[^\/]+)?", AdminPage),
    #messages stuff
    (r"/messages", MessagePage),
    (r"/sendmessage", SendMessage),
    #static file path handlers
    (r"/css/(.*)", tornado.web.StaticFileHandler, {'path': style_path}),
    (r"/images/(.*)", tornado.web.StaticFileHandler, {'path': img_path}),
    (r"/js/(.*)", tornado.web.StaticFileHandler, {'path': js_path}),
    (r"/fonts/(.*)", tornado.web.StaticFileHandler, {'path': fonts_path}),
    (r"/(.*)", tornado.web.StaticFileHandler, {'path': favicon_path}),
]

if __name__ == "__main__":
    application = tornado.web.Application(handlers)
    http_server = tornado.httpserver.HTTPServer(application)
    listen()
    tornado.ioloop.IOLoop.instance().start()
    print portconfig.logOutput