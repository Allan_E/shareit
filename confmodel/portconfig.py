from config import *

if state == "development":
        port = devPort
        instance = "production"
elif state == "production":
        port = productionPort
        instance = "production"
else:
        port = "ERROR"
        instance = "ERROR"

logOutput = "Running " + instance + " instance on port " + str(port)