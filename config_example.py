"""
Edit this file, then rename it to 'config.py'

"""
state = "development"
devPort = 8888 #default development port, you can change this if you like
productionPort = 80 #Default production port. Should be left as 80 because this is the standard HTTP port

db_user = '' #enter database username here
db_password = '' #database password
db_host = '' #database host. user 'localhost' unless told otherwise by hosting provider
db_db = '' #the database to use. Probably 'shareit' if you created the database with init.sql
