#imports n stuff
import tornado.template

#some vars we need throughout the confmodel
adminPageRaw = open('admin/page/admin.html').read()
adminPageTemplate = tornado.template.Template(adminPageRaw)

notAdminRaw = open('admin/page/no.html').read()
notAdminTemplate = tornado.template.Template(notAdminRaw)


def loadPage(self, headerTemplate, footerTemplate, action, db):

    username = self.get_cookie('username', '')
    storedRemkey = self.get_cookie('remkey', '')
    dbRemkey = ""
    admin = 0

    for i in db.query("SELECT remkey FROM users WHERE username=%s LIMIT 1", username):
        dbRemkey = i.remkey

    for a in db.query("SELECT admin FROM users WHERE username=%s LIMIT 1", username):
        admin = a.admin

    if username == "" or storedRemkey != dbRemkey:
        self.write(headerTemplate.generate(username="", db=db))
    else:
        self.write(headerTemplate.generate(username=username, db=db))
    #do stuff
    if action == "main" and storedRemkey == dbRemkey and admin == 1:
        err = self.get_argument('err', '')
        self.write(adminPageTemplate.generate(db=db, err=err))
    elif action == "delete" and storedRemkey == dbRemkey and admin == 1:
        pid = self.get_argument('id', '')
        if pid == "":
            self.redirect("/admin/main?err=1")
        else:
            db.execute("UPDATE links SET deleted=1 WHERE id=%s", pid)
        self.redirect("/admin/main")

    else:
        self.write(notAdminTemplate.generate(u=username))

     #render the footer while preventing anything else from rendering
    self.finish(footerTemplate.generate())
