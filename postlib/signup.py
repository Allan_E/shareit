import random
import string
import hashlib
import datetime

def signup(self, db):
        username = self.get_argument('username', '')
        password = self.get_argument('password', '')

        #generate random remkey
        remkey = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(255))

        if username=="" or password=="" or " " in username:
            self.redirect("/login?err=2")
        else:
            hashedPass = hashlib.md5(password).hexdigest()
            db.execute("INSERT INTO users (username, password, remkey) VALUES (%s, %s, %s)", username, hashedPass, remkey)
            expires = datetime.datetime.utcnow() + datetime.timedelta(days=365)
            self.set_cookie('username', username, expires=expires)
            self.set_cookie('remkey', remkey, expires=expires)
            self.redirect("/")