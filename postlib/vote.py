def up(self, db):
        pid = self.get_argument('pid', '')
        uname = self.get_cookie('username', '')
        
        for ndata in db.query("SELECT author FROM links WHERE id=%s", pid):
            author = ndata.author
        author = str(author)

        for adata in db.query("SELECT points FROM users WHERE username=%s", author):
            currPoints = adata.points

        if currPoints > 1 or currPoints == 0:
            currPoints = str(currPoints) + " Points"
        else:
            currPoints = str(currPoints) + " Point"

        hasVoted = db.execute_rowcount("SELECT * FROM voted WHERE pid=%s AND username=%s AND which=%s", pid, uname, "upvote")
        if hasVoted > 0:
            self.write(currPoints)
        else:
            for data in db.query("SELECT * FROM links WHERE id=%s", pid):
                curruvCount = data.upvotes
                currdvCount = data.downvotes

            curruvCount = str(curruvCount)
            curruvCount = int(curruvCount)
            newuvCount = curruvCount + 1

            currdvCount = str(currdvCount)
            currdvCount = int(currdvCount)

            for adata in db.query("SELECT points FROM users WHERE username=%s", author):
                currPoints = adata.points
            currPoints = str(currPoints)
            currPoints = int(currPoints)
            newPoints = currPoints + 1

            if pid=="" or uname=="":
                self.write("Error")
            else:
                db.execute("UPDATE links SET upvotes=%s WHERE id=%s", newuvCount, pid)
                db.execute("UPDATE users SET points=%s WHERE username=%s", newPoints, author)
                db.execute("INSERT INTO voted (pid, username, which) VALUES (%s, %s, %s)", pid, uname, "upvote")
                pointsCount = curruvCount - currdvCount
                if pointsCount > 1 or pointsCount == 0:
                    message = str(pointsCount) + " Points"
                else:
                    message = str(pointsCount) + " Point"
                self.write(message)


def down(self, db):
        pid = self.get_argument('pid', '')
        uname = self.get_cookie('username', '')

        for ndata in db.query("SELECT author FROM links WHERE id=%s", pid):
            author = ndata.author
        author = str(author)

        for adata in db.query("SELECT points FROM users WHERE username=%s", author):
            currPoints = adata.points
        if currPoints > 1 or currPoints == 0:
            currPoints = str(currPoints) + " Points"
        else:
            currPoints = str(currPoints) + " Point"

        hasVoted = db.execute_rowcount("SELECT * FROM voted WHERE pid=%s AND username=%s AND which=%s", pid, uname, "downvote")
        if hasVoted > 0:
            self.write(currPoints)
        else:
            for data in db.query("SELECT * FROM links WHERE id=%s", pid):
                currdvCount = data.downvotes
                curruvCount = data.upvotes

            currdvCount = str(currdvCount)
            currdvCount = int(currdvCount)
            newdvCount = currdvCount + 1

            curruvCount = str(curruvCount)
            curruvCount = int(curruvCount)

            for adata in db.query("SELECT points FROM users WHERE username=%s", author):
                currPoints = adata.points
            currPoints = str(currPoints)
            currPoints = int(currPoints)
            newPoints = currPoints - 1

            if pid=="" or uname=="":
                self.write("Error")
            else:
                db.execute("UPDATE links SET downvotes=%s WHERE id=%s", newdvCount, pid)
                db.execute("UPDATE users SET points=%s WHERE username=%s", newPoints, author)
                db.execute("INSERT INTO voted (pid, username, which) VALUES (%s, %s, %s)", pid, uname, "downvote")
                postPoints = curruvCount - currdvCount
                if postPoints > 1 or postPoints == 0:
                    message = str(postPoints) + " Points"
                else:
                    message = str(postPoints) + " Point"
                self.write(message)