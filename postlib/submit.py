def link(self, db):
        title = self.get_argument('title', '')
        link = self.get_argument('link', '')
        uname = self.get_cookie('username', '')
        storedRemkey = self.get_cookie('remkey', '')
        dbRemkey = ""
        subcat = self.get_argument('subcat', '')

        for i in db.query("SELECT remkey FROM users WHERE username=%s LIMIT 1", uname):
            dbRemkey = i.remkey

        ccount = db.execute_rowcount("SELECT * FROM subcats WHERE name=%s", subcat)

        if ccount < 1:
            scExists = "false"
        else:
            scExists = "true"

        if title == "" or link == "":
            self.redirect("/submit?err=1")
        elif uname == "" or storedRemkey != dbRemkey:
            self.redirect("/submit?err=2")
        elif subcat == "":
            self.redirect("/submit?err=3")
        elif scExists == "false":
            self.redirect("/submit?err=4")
        else:
            db.execute("INSERT INTO links (title, link, author, subcat) VALUES (%s, %s, %s, %s)", title, link, uname, subcat)
            pid = 0
            for i in db.query("SELECT id FROM links WHERE title = %s LIMIT 1", str(title)):
                pid = i.id

            db.execute("INSERT INTO voted (pid, username, which) VALUES (%s, %s, %s)", pid, uname, "upvote")
            self.redirect("/comments/" + str(pid))


def comment(self, db):
        parent = self.get_argument('parent', '')
        pid = self.get_argument('pid', '')
        comment = self.get_argument('comment', '')
        author = self.get_cookie('username', '')
        storedRemkey = self.get_cookie('remkey', '')
        dbRemkey = ""

        for i in db.query("SELECT remkey FROM users WHERE username=%s LIMIT 1", author):
            dbRemkey = i.remkey

        rdirStr = "/comments/" + pid

        """Check if they actually made a comment"""
        if comment == "":
            errStr = rdirStr + "?err=1"
            self.redirect(errStr)
        else:
            comment = comment

        if storedRemkey != dbRemkey:
            self.redirect(rdirStr)
        else:
            """If everything is OK, write to the database, and send us back to the comments page"""
            if parent == '':
                db.execute("INSERT INTO comments (pid, author, comment) VALUES (%s, %s, %s)", pid, author, comment)
                self.redirect(rdirStr)
            else:
                db.execute("INSERT INTO comments (pid, author, comment, parent) VALUES (%s, %s, %s, %s)", pid, author, comment, parent)
                self.redirect(rdirStr)