def create(self, db):
        name = self.get_argument('name', '')

        if name == "":
            self.redirect("/newcat?err=1")
        else:
            ename = db.query("SELECT name FROM subcats WHERE name=%s LIMIT 1", name)

            if name == ename:
                self.redirect("/newcat?err=2")
            elif name.isspace() or " " in name:
                self.redirect("/newcat?err=3")
            else:
                db.execute("INSERT INTO subcats (name) VALUES (%s)", name)
                self.redirect("/s/" + name)