def post(self, db):
        postid = self.get_argument('pid', '')
        uname = self.get_cookie('username')
        storedRemkey = self.get_cookie('remkey', '')
        dbRemkey = ""

        for i in db.query("SELECT remkey FROM users WHERE username=%s LIMIT 1", uname):
            dbRemkey = i.remkey

        if storedRemkey != dbRemkey:
            self.redirect("/")
        else:
            db.execute("UPDATE links SET deleted=1 WHERE id=%s AND author=%s", postid, uname)
            self.redirect('/')

def comment(self, db):
        commentid = self.get_argument('cid', '')
        uname = self.get_cookie('username')
        storedRemkey = self.get_cookie('remkey', '')
        dbRemkey = ""

        for i in db.query("SELECT remkey FROM users WHERE username=%s LIMIT 1", uname):
            dbRemkey = i.remkey

        if storedRemkey != dbRemkey:
            self.redirect("/")
        else:
            db.execute("UPDATE comments SET deleted=1 WHERE id=%s AND author=%s", commentid, uname)
            self.redirect("/")
