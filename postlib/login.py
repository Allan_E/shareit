import random
import string
import hashlib
import datetime

def login(self, db):
        username = self.get_argument('username', '')
        password = self.get_argument('password', '')
        remkey = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(255))

        if username=="" or password=="":
            self.redirect('/login?err=1')
        else:
            hashedPass = hashlib.md5(password).hexdigest()
            expires = datetime.datetime.utcnow() + datetime.timedelta(days=365)
            check = db.execute_rowcount("SELECT * FROM users WHERE username=%s AND password=%s", username, hashedPass)
            if check < 1:
                self.redirect("/login?err=1")
            else:
                db.execute("UPDATE users SET remkey=%s WHERE username=%s",remkey, username)
                self.set_cookie('username', username, expires=expires)
                self.set_cookie('remkey', remkey, expires=expires)
                self.redirect("/")