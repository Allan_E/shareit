def send(self, db):
    uname = self.get_cookie('username', '')
    storedremkey = self.get_cookie('remkey', '')
    dbremkey = ""
    for i in db.query("SELECT remkey FROM users WHERE username = %s", uname):
        dbremkey = i.remkey

    to = self.get_argument('to', '')
    body = self.get_argument('body', '')

    if storedremkey != dbremkey or to == "" or body == "":
        self.redirect("/messages")
    else:
        db.execute("INSERT INTO messages (msgto, msgfrom, body) VALUES (%s, %s, %s)", to, uname, body)
        self.redirect("/messages")